package veovismuaddib.mystcraft_info;

import net.minecraftforge.common.config.Config;
import net.minecraftforge.common.config.ConfigManager;
import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.common.*;
import net.minecraftforge.fml.common.event.*;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import org.apache.logging.log4j.Logger;
import veovismuaddib.mystcraft_info.proxy.CommonProxy;

@Mod(
    modid = ModMain.MODID,
    name = ModMain.NAME,
    version = ModMain.VERSION,
    dependencies = "required-after:mystcraft@[0.13.4.04,)"
)
public class ModMain {
    public static final String MODID = "mystcraft_info";
    public static final String NAME = "Mystcraft Info";
    public static final String VERSION = "1.0.3.0";

    @SidedProxy(
        clientSide = "veovismuaddib.mystcraft_info.proxy.ClientProxy",
        serverSide = "veovismuaddib.mystcraft_info.proxy.ServerProxy"
    )
    public static CommonProxy proxy;

    @Mod.Instance
    public static ModMain instance;

    public static Logger logger;

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent e) {
        logger = e.getModLog();
        proxy.preInit(e);
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent e) {
        proxy.init(e);
    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent e) {
        proxy.postInit(e);
    }

    @Mod.EventBusSubscriber
    private static class Events {
        @SubscribeEvent
        public static void onConfigChange(ConfigChangedEvent.OnConfigChangedEvent e) {
            if (e.getModID().equals(MODID)) {
                ConfigManager.sync(MODID, Config.Type.INSTANCE);
            }
        }
    }
}

package veovismuaddib.mystcraft_info.helpers;

import com.xcompwiz.mystcraft.api.symbol.IAgeSymbol;
import com.xcompwiz.mystcraft.api.word.WordData;
import net.minecraft.util.ResourceLocation;
import veovismuaddib.mystcraft_info.ModConfig;

import java.util.*;

public class PoemHelper {
    public static List<String> allWords = Arrays.asList(
        WordData.Balance,
        WordData.Believe,
        WordData.Celestial,
        WordData.Chain,
        WordData.Change,
        WordData.Chaos,
        WordData.Civilization,
        WordData.Constraint,
        WordData.Contradict,
        WordData.Control,
        WordData.Convey,
        WordData.Creativity,
        WordData.Cycle,
        WordData.Dependence,
        WordData.Discover,
        WordData.Dynamic,
        WordData.Elevate,
        WordData.Encourage,
        WordData.Energy,
        WordData.Entropy,
        WordData.Ethereal,
        WordData.Exist,
        WordData.Explore,
        WordData.Flow,
        WordData.Force,
        WordData.Form,
        WordData.Future,
        WordData.Growth,
        WordData.Harmony,
        WordData.Honor,
        WordData.Image,
        WordData.Infinite,
        WordData.Inhibit,
        WordData.Intelligence,
        WordData.Love,
        WordData.Machine,
        WordData.Merge,
        WordData.Momentum,
        WordData.Motion,
        WordData.Mutual,
        WordData.Nature,
        WordData.Nurture,
        WordData.Order,
        WordData.Possibility,
        WordData.Power,
        WordData.Question,
        WordData.Rebirth,
        WordData.Remember,
        WordData.Resilience,
        WordData.Resurrect,
        WordData.Sacrifice,
        WordData.Society,
        WordData.Spur,
        WordData.Static,
        WordData.Stimulate,
        WordData.Survival,
        WordData.Sustain,
        WordData.System,
        WordData.Terrain,
        WordData.Time,
        WordData.Tradition,
        WordData.Transform,
        WordData.Void,
        WordData.Weave,
        WordData.Wisdom
    );

    private static final HashMap<ResourceLocation, List<String>> poemCache = new HashMap<>();

    public static List<String> getPoemFromSymbol(IAgeSymbol symbol) {
        if(symbol == null) return null;

        ResourceLocation location = symbol.getRegistryName();
        if(poemCache.containsKey(location)) {
            return poemCache.get(location);
        }

        List<String> poem = new ArrayList<>();
        String replacement = null;

        boolean replaceWithName = ModConfig.poems.replace_with_name;
        boolean replaceWithQuestionMarks = ModConfig.poems.replace_with_question_marks;

        if(replaceWithQuestionMarks || replaceWithName) {

            replacement = replaceWithName ? symbol.getLocalizedName() : "???";
        }

        for(String word: symbol.getPoem()) {
            if(replacement == null || allWords.contains(word)) {
                poem.add(word);
            } else {
                poem.add(replacement);
            }
        }

        poemCache.put(location, poem);
        return poem;
    }
}

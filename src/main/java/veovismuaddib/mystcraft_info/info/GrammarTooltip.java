package veovismuaddib.mystcraft_info.info;

import com.xcompwiz.mystcraft.api.symbol.IAgeSymbol;
import veovismuaddib.mystcraft_info.ModConfig;
import veovismuaddib.mystcraft_info.helpers.GrammarHelper;

import java.util.ArrayList;
import java.util.List;

public class GrammarTooltip extends SymbolTooltip {
    @Override
    public boolean isEnabled() {
        return ModConfig.tooltips.grammar.enabled;
    }

    @Override
    public boolean requiresShift() {
        return ModConfig.tooltips.grammar.shift_required;
    }

    public GrammarTooltip() {
        setHeader("gui.mystcraft_info.grammar.header");
    }

    @Override
    public List<String> getTooltip(IAgeSymbol symbol) {
        List<String> tooltip = new ArrayList<>();

        if(!isVisible(symbol)) return tooltip;

        List<String> rules = GrammarHelper.getGrammarFromSymbol(symbol);

        if(!rules.isEmpty()) {
            // This should be locale sensitive.
            rules.sort(String::compareToIgnoreCase);

            tooltip = super.getTooltip(symbol);
            for(String rule: rules) {
                tooltip.add(DEFAULT_FORMAT + rule);
            }
        }

        return tooltip;
    }
}

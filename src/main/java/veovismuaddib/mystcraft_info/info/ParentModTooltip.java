package veovismuaddib.mystcraft_info.info;

import java.util.List;

import com.xcompwiz.mystcraft.api.symbol.IAgeSymbol;

import net.minecraft.block.state.IBlockState;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.biome.Biome;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.ModContainer;
import veovismuaddib.mystcraft_info.ModConfig;
import veovismuaddib.mystcraft_info.helpers.ModifierHelper;

public class ParentModTooltip extends SymbolTooltip {

    private static final String FORMAT_STANDARD = TextFormatting.RESET.toString() + TextFormatting.GRAY.toString();
    private static final String FORMAT_MOD = TextFormatting.RESET.toString() + TextFormatting.BLUE.toString()
            + TextFormatting.ITALIC.toString();
    private static final String FORMAT_MOD_SYMBOL = TextFormatting.RESET.toString() + TextFormatting.DARK_GREEN.toString()
            + TextFormatting.ITALIC.toString();

    @Override
    public boolean isEnabled() {
        return ModConfig.tooltips.parent_mod.enabled;
    }

    @Override
    public boolean requiresShift() {
        return ModConfig.tooltips.parent_mod.shift_required;
    }

    public List<String> getTooltip(IAgeSymbol symbol) {
        List<String> tooltip = super.getTooltip(symbol);

        if(!isVisible(symbol)) return tooltip;

        // The modName for the thing added by the symbol
        String modName = getBlockModName(symbol);
        modName = modName != null ? modName : getBiomeModName(symbol);

        // The modName that added the symbol
        String symbolModName = getSymbolModName(symbol);

        // If modName is still null, fallback to symbolModName
        modName = modName != null ? modName : symbolModName;

        if(modName.equals(symbolModName)) {
            tooltip.add(FORMAT_MOD + symbolModName);
        } else {
            String langKey = "mystcraft_info.tooltips.format.parent_mod_multiple";
            modName = FORMAT_MOD + modName + FORMAT_STANDARD;
            symbolModName = FORMAT_MOD_SYMBOL + symbolModName + FORMAT_STANDARD;

            tooltip.add(FORMAT_STANDARD + I18n.format(langKey,symbolModName, modName));
        }

        return tooltip;
    }

    private static String resourceDomainToModName(String modId) {
        String modName = null;
        for(ModContainer mod : Loader.instance().getModList()) {
            if(mod.getModId().equals(modId)) {
                modName = mod.getName();
                break;
            }
        }
        return modName;
    }

    private static String getSymbolModName(IAgeSymbol symbol) {
        String modId = symbol.getRegistryName().getResourceDomain();
        return resourceDomainToModName(modId);
    }

    private static String getBiomeModName(IAgeSymbol symbol) {
        Biome biome = ModifierHelper.getBiomeFromSymbol(symbol);
        String modId = "";
        if(biome != null) {
            modId = ModifierHelper.getBiomeFromSymbol(symbol).getRegistryName().getResourceDomain();
        }
        return resourceDomainToModName(modId);
    }

    private static String getBlockModName(IAgeSymbol symbol) {
        IBlockState state = ModifierHelper.getBlockStateFromSymbol(symbol);
        String modId = "";
        if(state != null) {
            modId = ModifierHelper.getBlockStateFromSymbol(symbol).getBlock().getRegistryName().getResourceDomain();
        }
        return resourceDomainToModName(modId);
    }
}
package veovismuaddib.mystcraft_info.listeners;

import com.xcompwiz.mystcraft.api.symbol.IAgeSymbol;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.event.entity.player.ItemTooltipEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import veovismuaddib.mystcraft_info.*;
import veovismuaddib.mystcraft_info.info.SymbolTooltip;

import java.util.ArrayList;
import java.util.List;

public class PageTooltipListener {
    private static final String SHIFT_FORMATTING = TextFormatting.BLUE.toString() + TextFormatting.ITALIC;

    private List<SymbolTooltip> symbolTooltips = new ArrayList<>();

    public void addTooltip(SymbolTooltip tooltip) { symbolTooltips.add(tooltip); }

    @SubscribeEvent
    public void onTooltip(ItemTooltipEvent e) {
        List<String> tooltip = e.getToolTip();

        ItemStack item = e.getItemStack();
        if(item == null) {
            return;
        }

        ResourceLocation symbolLocation = ModMain.proxy.pageApi.getPageSymbol(item);
        if(symbolLocation == null) {
            return;
        }

        IAgeSymbol symbol = ModMain.proxy.symbolRegistry.getValue(symbolLocation);
        if(symbol == null) {
            return;
        }

        boolean shiftHidden = false;
        for (SymbolTooltip symbolTooltip : symbolTooltips) {
            tooltip.addAll(symbolTooltip.getTooltip(symbol));
            if(!GuiScreen.isShiftKeyDown()) {
                if(symbolTooltip.isRelevant(symbol) && symbolTooltip.requiresShift()) {
                    shiftHidden = true;
                }
            }
        }

        if(shiftHidden) {
            tooltip.add("");
            tooltip.add(SHIFT_FORMATTING + ModMain.proxy.localize("gui.mystcraft_info.shift_required"));
        }
    }
}
